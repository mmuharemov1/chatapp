/*
Navicat MySQL Data Transfer

Source Server         : ChatAplikacija
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : chataplikacija

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2015-04-28 22:22:39
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `korisnik`
-- ----------------------------
DROP TABLE IF EXISTS `korisnik`;
CREATE TABLE `korisnik` (
  `ID_korisnika` int(11) NOT NULL auto_increment,
  `Ime` text NOT NULL,
  `Prezime` text NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Passwd` text NOT NULL,
  `login_status` int(11) NOT NULL default '0',
  `email` varchar(50) NOT NULL,
  PRIMARY KEY  (`ID_korisnika`),
  UNIQUE KEY `Unique` USING BTREE (`email`,`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of korisnik
-- ----------------------------
INSERT INTO `korisnik` VALUES ('1', 'Adel', 'Sabic', 'adel123', 'adel123', '0', 'adel.asd');
INSERT INTO `korisnik` VALUES ('2', 'Mirza', 'Muharemovic', 'mirza_m', 'mirza456', '0', 'mirza_muharemovic@live.com');
INSERT INTO `korisnik` VALUES ('11', 'Mujo', 'Mujić', 'mujo123', 'mujo123', '0', 'mujo_mujic@live.com');

-- ----------------------------
-- Table structure for `poruka`
-- ----------------------------
DROP TABLE IF EXISTS `poruka`;
CREATE TABLE `poruka` (
  `ID_poruke` int(11) NOT NULL auto_increment,
  `ID_korisnika` int(11) NOT NULL,
  `Poruka` longtext NOT NULL,
  PRIMARY KEY  (`ID_poruke`),
  KEY `Korisnik` (`ID_korisnika`),
  CONSTRAINT `Korisnik` FOREIGN KEY (`ID_korisnika`) REFERENCES `korisnik` (`ID_korisnika`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of poruka
-- ----------------------------
INSERT INTO `poruka` VALUES ('37', '11', 'Zdravo ');
