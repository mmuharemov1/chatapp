﻿namespace Chat_aplikacija_server
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.korisnici = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IPadrese = new System.Windows.Forms.ListBox();
            this.slanje_privatne_poruke = new System.Windows.Forms.Button();
            this.slanje_poruke_svima = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.diskonekcija = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // korisnici
            // 
            this.korisnici.FormattingEnabled = true;
            this.korisnici.Location = new System.Drawing.Point(12, 22);
            this.korisnici.Name = "korisnici";
            this.korisnici.Size = new System.Drawing.Size(135, 329);
            this.korisnici.TabIndex = 0;
            this.korisnici.SelectedIndexChanged += new System.EventHandler(this.korisnici_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Korisničko ime:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(162, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "IP adresa korisnika:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // IPadrese
            // 
            this.IPadrese.FormattingEnabled = true;
            this.IPadrese.Location = new System.Drawing.Point(165, 22);
            this.IPadrese.Name = "IPadrese";
            this.IPadrese.Size = new System.Drawing.Size(135, 329);
            this.IPadrese.TabIndex = 4;
            this.IPadrese.SelectedIndexChanged += new System.EventHandler(this.IPadrese_SelectedIndexChanged);
            // 
            // slanje_privatne_poruke
            // 
            this.slanje_privatne_poruke.Location = new System.Drawing.Point(316, 388);
            this.slanje_privatne_poruke.Name = "slanje_privatne_poruke";
            this.slanje_privatne_poruke.Size = new System.Drawing.Size(288, 23);
            this.slanje_privatne_poruke.TabIndex = 8;
            this.slanje_privatne_poruke.Text = "Pošalji privatnu poruku";
            this.slanje_privatne_poruke.UseVisualStyleBackColor = true;
            this.slanje_privatne_poruke.Click += new System.EventHandler(this.slanje_privatne_poruke_Click);
            // 
            // slanje_poruke_svima
            // 
            this.slanje_poruke_svima.Location = new System.Drawing.Point(12, 359);
            this.slanje_poruke_svima.Name = "slanje_poruke_svima";
            this.slanje_poruke_svima.Size = new System.Drawing.Size(288, 23);
            this.slanje_poruke_svima.TabIndex = 9;
            this.slanje_poruke_svima.Text = "Pošalji poruku svima";
            this.slanje_poruke_svima.UseVisualStyleBackColor = true;
            this.slanje_poruke_svima.Click += new System.EventHandler(this.broadcast_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(316, 22);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(288, 290);
            this.textBox1.TabIndex = 11;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(313, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Sadržaj dopisivanja:";
            // 
            // diskonekcija
            // 
            this.diskonekcija.Location = new System.Drawing.Point(12, 388);
            this.diskonekcija.Name = "diskonekcija";
            this.diskonekcija.Size = new System.Drawing.Size(288, 23);
            this.diskonekcija.TabIndex = 13;
            this.diskonekcija.Text = "Diskonektuj korisnika";
            this.diskonekcija.UseVisualStyleBackColor = true;
            this.diskonekcija.Click += new System.EventHandler(this.diskonekcija_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(316, 326);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(287, 55);
            this.textBox2.TabIndex = 14;
            this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyDown);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(616, 419);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.diskonekcija);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.slanje_poruke_svima);
            this.Controls.Add(this.slanje_privatne_poruke);
            this.Controls.Add(this.IPadrese);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.korisnici);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox korisnici;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox IPadrese;
        private System.Windows.Forms.Button slanje_privatne_poruke;
        private System.Windows.Forms.Button slanje_poruke_svima;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button diskonekcija;
        private System.Windows.Forms.TextBox textBox2;
    }
}

