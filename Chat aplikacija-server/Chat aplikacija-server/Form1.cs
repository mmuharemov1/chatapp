﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworksApi.TCP.SERVER;

namespace Chat_aplikacija_server
{
    public delegate void UpdateChatLog(string txt);
    public delegate void UpdateListbox(ListBox box,string value, bool ukloni);

    public partial class Form1 : Form
    {
        Server server;
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            server=new Server("192.168.1.5","90");
            server.OnClientConnected +=new OnConnectedDelegate(server_OnClientConnected);
            server.OnClientDisconnected +=new OnDisconnectedDelegate(server_OnClientDisconnected);
            server.OnDataReceived +=new OnReceivedDelegate(server_OnDataReceived);
            server.OnServerError +=new OnErrorDelegate(server_OnServerError);
            server.Start();


        }

        private void IzmjenaSadrzajaDop(string txt)
        {
            if (textBox1.InvokeRequired)
            {
                Invoke(new UpdateChatLog(IzmjenaSadrzajaDop), new object[] { txt });
            }
            else textBox1.Text += txt + "\r\n";
        }

        private void IzmjenaListBox(ListBox box,string value,bool ukloni)
        {
            if (box.InvokeRequired)
            { 
              Invoke(new UpdateListbox(IzmjenaListBox),new object[] {box,value,ukloni});
            }else
            {
                if (ukloni)
                {
                    box.Items.Remove(value);
                }
                else
                {
                    box.Items.Add(value);
                }
            }
        
        } 
    

        void server_OnServerError(object Sender, ErrorArguments R)
        {
            MessageBox.Show(R.ErrorMessage);

        }

        void server_OnDataReceived(object Sender, ReceivedArguments R)
        {
            IzmjenaSadrzajaDop(R.Name + " kaze: " + R.ReceivedData);
            server.BroadCast(R.Name + " kaze: " + R.ReceivedData);

        }

        void server_OnClientDisconnected(object Sender, DisconnectedArguments R)
        {
            server.BroadCast(R.Name + " je napustio konverzaciju");
            IzmjenaListBox(korisnici, R.Name, true);
            IzmjenaListBox(IPadrese, R.Ip, true);

        }

        void server_OnClientConnected(object Sender, ConnectedArguments R)
        {
            server.BroadCast(R.Name + " se priključio konverzaciji");
            IzmjenaListBox(korisnici, R.Name, false);
            IzmjenaListBox(IPadrese, R.Ip, false);
        }

        private void slanje_privatne_poruke_Click(object sender, EventArgs e)
        {

            if (textBox2.Text != "") server.SendTo((string)korisnici.SelectedItem, "Administrator kaže: " + textBox2.Text);
         
        }

        private void broadcast_Click(object sender, EventArgs e)
        {
            if(textBox2.Text!="") server.BroadCast("Administrator kaže: "+textBox2.Text);
            textBox2.Clear();
           
        }

        private void diskonekcija_Click(object sender, EventArgs e)
        {
             server.DisconnectClient((string)korisnici.SelectedItem);
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);

        }

        private void korisnici_SelectedIndexChanged(object sender, EventArgs e)
        {
            IPadrese.SelectedIndex = korisnici.SelectedIndex;
        }

        private void IPadrese_SelectedIndexChanged(object sender, EventArgs e)
        {
            korisnici.SelectedIndex = IPadrese.SelectedIndex;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
