﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Net.Mail;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static String ID_korisnika;
        public static String imePrez;
        public static String korisnickoIme;
        public static String email;
        public static String password;



        public static String konekcioniString = "Server=localhost; Port=3306; " +
            "Database=chataplikacija; Uid=root; Pwd=root";

        private void RegistracijaKorisnika()
        {
            try
            {
                if (textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "" || textBox6.Text == "" || textBox7.Text == "")
                    MessageBox.Show("Niste unijeli potrebne podatke za registraciju !");
                else
                {

                    String upit = "INSERT INTO korisnik(Ime, Prezime, Username, Passwd, email, login_status) " +
                        "VALUES " +
                        " ('" + textBox3.Text + "', '" + textBox4.Text + "', '" + textBox5.Text + "', " +
                        " '" + textBox6.Text + "', '" + textBox7.Text + "', 0) ";

                    MySqlConnection konekcija = new MySqlConnection(konekcioniString);
                    konekcija.Open();

                    MySqlCommand cmd = new MySqlCommand(upit, konekcija);

                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Uspješno ste se registrovali! Pritisnite 'OK' za prijavu", "Registracija uspješna", MessageBoxButtons.OK);

                    konekcija.Close();

                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox6.Text = "";
                    textBox7.Text = "";
                    // Vraćanje na prijavu
                    
                    textBox1.Visible = true;
                    textBox2.Visible = true;
                    label1.Visible = true;
                    label2.Visible = true;
                    button1.Visible = true;
                    button2.Visible = false;
                    button3.Visible = false;
                    button5.Visible = true;
                    button4.Visible = false;
                    button6.Visible = false;
                    //
                    textBox3.Visible = false;
                    textBox4.Visible = false;
                    textBox5.Visible = false;
                    textBox6.Visible = false;
                    textBox7.Visible = false;
                    // 
                    label3.Visible = false;
                    label4.Visible = false;
                    label5.Visible = false;
                    label6.Visible = false; 
                    label7.Visible = false;


                }

            }
            catch (Exception greska)
            {
                MessageBox.Show(greska.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            korisnickoIme = textBox1.Text;
            String sifra = textBox2.Text;
            String query;


            query = "SELECT passwd, CONCAT(ime, ' ', prezime),login_status,ID_korisnika,email " +
                "FROM korisnik WHERE Username ='" + korisnickoIme + "';";

            try
            {
                MySqlConnection konekcija = new MySqlConnection(konekcioniString);

                konekcija.Open();

                MySqlCommand cmd = new MySqlCommand(query, konekcija);

                MySqlDataReader reader;
                reader = cmd.ExecuteReader();
                reader.Read();

                if (!reader.HasRows)
                {
                    errorProvider1.SetError(textBox1, "Pogrešno korisničko ime !!!");
                }
                else
                {
                    password = reader[0].ToString();
                    imePrez = reader[1].ToString();
                    String loginStatus = reader[2].ToString();
                    ID_korisnika = reader[3].ToString();
                    email = reader[4].ToString();


                    if (loginStatus == "1")
                    {
                        errorProvider1.SetError(button1, "Korisnik je već logovan !!!");
                    }
                    else if (sifra == password)
                    {
                        MessageBox.Show("Uspješno ste logovani " + imePrez);
                        PostaviStatusLogin();
                        Form2 fr2 = new Form2();
                        this.Hide();
                        fr2.Show();
                    }
                    else
                    {
                        errorProvider1.SetError(textBox2, "Pogrešan password !!!");
                    }
                }

                reader.Close();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void PostaviStatusLogin()
        {


            String query = "UPDATE korisnik SET login_status=1 " +
                           "WHERE ID_korisnika=" + ID_korisnika + ";";



            try
            {
                MySqlConnection konekcija = new MySqlConnection(konekcioniString);
                konekcija.Open();

                MySqlCommand cmd = new MySqlCommand(query, konekcija);


                cmd.ExecuteNonQuery();

                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void PostaviStatusLogout()
        {
            String query = "UPDATE korisnik SET login_status=0 " +
                           "WHERE ID_korisnika=" + ID_korisnika + ";";

            try
            {
                MySqlConnection konekcija = new MySqlConnection(konekcioniString);
                konekcija.Open();

                MySqlCommand cmd = new MySqlCommand(query, konekcija);

                cmd.ExecuteNonQuery();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox2.Visible = true;
            label1.Visible = true;
            label2.Visible = true;
            button1.Visible = true;
            button2.Visible = false;
            button3.Visible = false;
            button5.Visible = true;
          
            pictureBox1.Visible = false;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox3.Visible = true;
            textBox4.Visible = true;
            textBox5.Visible = true;
            textBox6.Visible = true;
            textBox7.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = true;
            button6.Visible = true;
            pictureBox1.Visible = false;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Visible = false;
            textBox2.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
            button1.Visible = false;
            button2.Visible = true;
            button3.Visible = true;
            button5.Visible = false;
           
            pictureBox1.Visible = true;

        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            textBox6.Visible = false;
            textBox7.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            button2.Visible = true;
            button3.Visible = true;
            button4.Visible = true;
            button6.Visible = false;
            button4.Visible = false;
            pictureBox1.Visible = true;



        }

        private void button4_Click(object sender, EventArgs e)
        {
            RegistracijaKorisnika();

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

      




        }



    }


