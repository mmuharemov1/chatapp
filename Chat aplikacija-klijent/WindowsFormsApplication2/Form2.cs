﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworksApi.TCP.CLIENT;
using MySql.Data.MySqlClient; 


namespace WindowsFormsApplication2
{
    public delegate void UpdateText(string txt);
    public partial class Form2 : Form
    {
        Client korisnik;
       
        public Form2()
        {
            InitializeComponent();
        }

        
        
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 fr1 = new Form1();
            fr1.PostaviStatusLogout();
            Application.Exit();
        }
        private void IzmjenaSadrzaja(string txt)
        {
            if (textBox3.InvokeRequired)
            {
                Invoke(new UpdateText(IzmjenaSadrzaja), new object[] { txt });

            }
            else
            {
                textBox3.Text += txt + "\r\n";
            }
        }
        void korisnik_OnDataReceived(object Sender, ClientReceivedArguments R)
        {
            IzmjenaSadrzaja(R.ReceivedData);

        }

        void korisnik_OnClientError(object Sender, ClientErrorArguments R)
        {
            IzmjenaSadrzaja(R.ErrorMessage);
        }
        void korisnik_OnClientDisconnected(object Sender, ClientDisconnectedArguments R)
        {
            IzmjenaSadrzaja("Diskonektovani ste!");
            diskonekcija();
        }

        void korisnik_OnClientConnecting(object Sender, ClientConnectingArguments R)
        {
            IzmjenaSadrzaja("Povezivanje...");
        }
        void korisnik_OnClientConnected(object Sender, ClientConnectedArguments R)
        {
            IzmjenaSadrzaja("Povezani ste!");
        }



        private void Form2_Load(object sender, EventArgs e)
        {
         
            label1.Text = Form1.imePrez;
            korisnik = new Client();
                korisnik.ClientName = Form1.korisnickoIme;
                korisnik.ServerIp = "192.168.1.5";
                korisnik.ServerPort = "90";

                korisnik.OnClientConnected += new OnClientConnectedDelegate(korisnik_OnClientConnected);
                korisnik.OnClientConnecting +=new OnClientConnectingDelegate(korisnik_OnClientConnecting);
                korisnik.OnClientDisconnected +=new OnClientDisconnectedDelegate(korisnik_OnClientDisconnected);
                korisnik.OnClientError +=new OnClientErrorDelegate(korisnik_OnClientError);
           
                korisnik.OnDataReceived +=new OnClientReceivedDelegate(korisnik_OnDataReceived);

                korisnik.Connect();
        }

        private void slanje_Click(object sender, EventArgs e)
        {
            if (korisnik != null && korisnik.IsConnected&&textBox1.Text!="")
            {
                korisnik.Send(textBox1.Text);
               
               
                String upit_poruke = "INSERT INTO poruka(ID_korisnika, Poruka) " +
                        "VALUES " +
                        " ('" + Form1.ID_korisnika.ToString() + "', '" + textBox1.Text +" ') ";

                MySqlConnection konekcija = new MySqlConnection(Form1.konekcioniString);
                konekcija.Open();

                MySqlCommand cmd = new MySqlCommand(upit_poruke,konekcija);

                cmd.ExecuteNonQuery();
                
                

                konekcija.Close();
                textBox1.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox3.Clear();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (korisnik != null && korisnik.IsConnected && e.KeyCode == Keys.Enter)
            {
                korisnik.Send(textBox1.Text);
                String upit_poruke = "INSERT INTO poruka(ID_korisnika, Poruka) " +
                         "VALUES " +
                         " ('" + Form1.ID_korisnika.ToString() + "', '" + textBox1.Text + " ') ";

                MySqlConnection konekcija = new MySqlConnection(Form1.konekcioniString);
                konekcija.Open();

                MySqlCommand cmd = new MySqlCommand(upit_poruke, konekcija);

                cmd.ExecuteNonQuery();
             


                konekcija.Close();

                textBox1.Clear();
            }
        }

        private void diskonekcija()
        {

            korisnik.Disconnect();
            String query = "UPDATE korisnik SET login_status=0 " +
                            "WHERE ID_korisnika=" + Form1.ID_korisnika + ";";

            try
            {
                MySqlConnection konekcija = new MySqlConnection(Form1.konekcioniString);
                konekcija.Open();

                MySqlCommand cmd = new MySqlCommand(query, konekcija);

                cmd.ExecuteNonQuery();
                konekcija.Close();
                Form1 fr3 = new Form1();
                this.Hide();
                fr3.Show();
               
                
            }
            catch 

            {
               
            }
            
         

        }
        private void button2_Click(object sender, EventArgs e)
        {
            diskonekcija();
        }
        
      

        private void Form2_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            diskonekcija();
            System.Environment.Exit(System.Environment.ExitCode);
        }
    
    }
}
